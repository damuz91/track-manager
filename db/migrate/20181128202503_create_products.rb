class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :status, default: Product.statuses[:active]
      t.integer :product_type, default: Product.product_types[:free]
      t.text :description

      t.timestamps
    end
  end
end
