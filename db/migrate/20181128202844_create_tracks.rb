class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.references :product, foreign_key: true
      t.integer :status, default: Track.statuses[:active]
      t.string :name
      t.integer :track_type, default: Track.track_types[:heatup]
      t.integer :duration, default: 0

      t.timestamps
    end
  end
end
