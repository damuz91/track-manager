class CreateUserProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :user_products do |t|
      t.references :user, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :status, default: UserProduct.statuses[:active]

      t.timestamps
    end
  end
end
