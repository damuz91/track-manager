class AddAttachmentGuideToProducts < ActiveRecord::Migration[5.1]
  def self.up
    change_table :products do |t|
      t.attachment :guide
    end
  end

  def self.down
    remove_attachment :products, :guide
  end
end
