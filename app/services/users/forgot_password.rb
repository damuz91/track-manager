class Users::ForgotPassword

    # Attempts to send user password recovery instructions
    # Params:
    # email*
    def self.call(params)
        email = params[:email].downcase rescue ""

        if Utils::Validator.validate_fields([:email], params)
            response = Utils::Response.get_error("Missing parameters.", nil)
            status = 403
            return {response: response, status: status}
        end

        if email.blank?
            response = Utils::Response.get_error("Email is required")
            status = 403
        else
            user = User.where(email: email).first
            if user.nil?
                response = Utils::Response.get_error("User not found")
                status = 404
            else
                response = Utils::Response.get_success("Reset password instructions sent", {})
                status = 200
                user.send_reset_password_instructions
            end
        end

        return {response: response, status: status}
    end
end