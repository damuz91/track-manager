class Users::Login

	# Attempts to login a user by comparing its email and password
    # Returns a JWT with the user_id wrapped inside it
    def self.call(params)

        if Utils::Validator.validate_fields([:email, :password], params)
      		response = Utils::Response.get_error("Missing parameters.", nil)
      		status = 403
      		return {response: response, status: status}
    	end

    	email = params[:email].downcase rescue ""
        password = params[:password]

        user = User.where(email: email).first
        if user.nil?
            response = Utils::Response.get_error("User not found")
            status = 404
        else
            if user.valid_password?(password)
                response = Utils::Response.get_success("Successfully logged in", {token: Security::GenerateToken.call({user_id: user.id})})
                status = 200
            else
                response = Utils::Response.get_error("User not found")
                status = 404
            end
        end

        return {response: response, status: status}
	end

end