class Users::Register

	# Registers a new user
	# Params:
	# first_name*
	# last_name*
	# email*
	# password*
    # Returns a JWT if succeded 
	def self.call(params)

		if Utils::Validator.validate_fields([:first_name, :last_name, :email, :password], params)
      		response = Utils::Response.get_error("Missing parameters.", nil)
      		status = 403
      		return {response: response, status: status}
    	end

		first_name = params[:first_name]
        last_name = params[:last_name]
        email = params[:email].downcase
        password = params[:password]
        
        user = User.new(:name => name, :email => email, :password => password, :password_confirmation => password, :role => User::Roles[:normal])
        if user.save
            response = Utils::Response.get_success("You have registered successfully", {token: Security::GenerateToken.call({:user_id => user.id}) })
            status = 200
        else
            response = Utils::Response.get_error("Error while registering", user.errors.full_messages)
            status = 403
        end

        return {response: response, status: status}
	end
end