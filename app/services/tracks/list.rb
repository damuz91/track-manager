class Tracks::List

	# List all available tracks by a given product
	# Params:
	# product_id*
	# current_user*
	# page
	# limit
	def self.call(params)
		page = params[:page].present? ? params[:page].to_i : 1
		limit = params[:limit].present? ? params[:limit].to_i : 50
		product_id = params[:product_id]

		if Utils::Validator.validate_fields([:product_id, :current_user], params)
      		response = Utils::Response.get_error("Missing parameters.", nil)
      		status = 403
      		return {response: response, status: status}
    	end

		tracks = Track.where(status: Track.statuses[:active], product_id: product_id).page(page).limit(limit)
		data = tracks.map { |track| Tracks::ListPresenter.new(track) }

		response = Utils::Response.get_success("", {data: tracks})
		status = 200

		return {response: response, status: status}
	end

end