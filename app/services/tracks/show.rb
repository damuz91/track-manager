class Tracks::Show

	# List all available tracks by a given product
	# Params:
	# track_id*
	# current_user*
	# page
	# limit
	def self.call(params)
		
		track_id = params[:track_id]

		if Utils::Validator.validate_fields([:track_id, :current_user], params)
      		response = Utils::Response.get_error("Missing parameters.", nil)
      		status = 403
      		return {response: response, status: status}
    	end

		track = Track.find(track_id)

		response = Utils::Response.get_success("", {data: track})
		status = 200

		return {response: response, status: status}
	end

end