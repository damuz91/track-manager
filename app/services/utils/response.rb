class Utils::Response

  def self.get_success(message,data)
    {success: true, message:message, data:data}
  end

  def self.get_error(message,data=nil)
    {success: false, message: message, data:data}
  end
end