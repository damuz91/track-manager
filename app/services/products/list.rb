class Products::List

	# List all available products
	# Params:
	# current_user*
	# page
	# limit
	def self.call(params)
		page = params[:page].present? ? params[:page].to_i : 1
		limit = params[:limit].present? ? params[:limit].to_i : 50

		if Utils::Validator.validate_fields([:current_user], params)
      		response = Utils::Response.get_error("Missing parameters.", nil)
      		status = 403
      		return {response: response, status: status}
    	end

    	current_user = params[:current_user]

		products = Product.where(status: Product.statuses[:active]).page(page).limit(limit)
		data = products.map { |product| Products::ListPresenter.new(product, current_user) }

		response = Utils::Response.get_success("", {products: data})
		status = 200

		return {response: response, status: status}
	end

end