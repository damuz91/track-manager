class Security::GenerateToken

    # Generates a JWT from and wraps the user id inside it
    def self.call(params)
        payload = {
            :uid => params[:user_id],
            :iss => 'TrackManager',
            :sub => '',
            :exp => calculate_expiration_time
        }
        token = JWT.encode payload, ENV['TOKEN_SECRET_SIGN'], 'HS256'
        return token
    end

    private
    def self.calculate_expiration_time
        9999999999 # TODO: Add reasonable TTL
    end

end
