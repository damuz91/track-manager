class Api::RegistrationsController < ApiController

    # Registers a new user
    # 
    api :POST, '/register', "Registers a new user. Email should be unique, all fields required"
    param :email, String, required: true
    param :first_name, String, required: true
    param :last_name, String, required: true
    param :password, String, required: true
    def create
        response = Users::Register.call({email: params[:email], first_name: params[:first_name], last_name: params[:last_name], password: params[:password]})
        render json: {response: response[:response], status: response[:status]}
    end

end
