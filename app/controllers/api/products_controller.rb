class Api::ProductsController < ApiController

	before_action :check_session

	# List available products
	# Params:
	# current_user*
	# page
	# limit
	api :GET, '/products', "List all available products for requesting user"
	def index
		response = Products::List.call({current_user: current_user, limit: params[:limit], page: params[:page]})
        render json: {response: response[:response], status: response[:status]}
	end
end