class Api::SessionsController < ApiController

    # Logins the user
    # Params:
    # email*
    # password*
    api :POST, '/login', "Attempts to login with given email and password. Returns JWT if succeeded"
    param :email, String, required: true
    param :password, String, required: true
    def create
        response = Users::Login.call({email: params[:email], password: params[:password]})
        render json: {response: response[:response], status: response[:status]}
    end

    # Send user forgot password recovery instructions
    # Params:
    # email*
    api :POST, '/forgot-password', "Sends forgot password instructions"
    param :email, String, required: true
    def forgot_password
        response = Users::ForgotPassword.call({email: params[:email], password: params[:password]})
        render json: {response: response[:response], status: response[:status]}
    end
    
end
