class Api::TracksController < ApiController

	before_action :check_session

	# List available tracks
	# Params:
	# current_user*
	# product_id
	# page
	# limit
	api :GET, '/tracks', "List all available products for requesting user"
	param :product_id, String, "The Product id", required: true
	def index
		response = Tracks::List.call({current_user: current_user, product_id: params[:product_id], limit: params[:limit], page: params[:page]})
        render json: {response: response[:response], status: response[:status]}
	end

	api :GET, '/track-detail', "Show specific track information"
	param :track_id, String, "The track id", required: true
	def show
		response = Tracks::Show.call({current_user: current_user, track_id: params[:track_id]})
        render json: {response: response[:response], status: response[:status]}
	end
end