class Admin::UsersController < Admin::AdminController

  before_action :set_user, except: [:index, :new, :create]

  def index
    @users = User.all
  end

  def new
    @user = User.new
    render '_form', layout: false
  end

  def create
    @user = User.new user_params
    @user.password, @user.password_confirmation = @user.email
    if @user.save
      @success = true
      flash[:notice] = 'Se creó el usuario exitosamente'
    else
      @success = false
      @errors = @user.errors.full_messages
    end
    render 'admin/partials/_generic_js_response', layout: false
  end

  def edit
    render '_form', layout: false
  end

  def update
    if @user.update_attributes user_params
      @success = true
      flash[:notice] = 'Se actualizó el usuario exitosamente'
    else
      @success = false
      @errors = @user.errors.full_messages
    end
    render 'admin/partials/_generic_js_response', layout: false
  end

  def destroy
    if @user.destroy
      @success = true
      flash[:notice] = 'Se eliminó el usuario exitosamente'
    else
      @success = false
      @errors = @user.errors.full_messages
    end
    render 'admin/partials/_generic_js_response', layout: false
  end

  def products
  end

  def all_products
    @products = Product.where(status: Product.statuses[:active])
    render '_all_products', layout: false
  end

  def assign_product
    up = UserProduct.new(user_id: @user.id, product_id: params[:user_product][:product_id])
    if up.save
      @success = true
      flash[:notice] = 'Se asignó el producto al usuario'
    else
      @success = false
      @errors = up.errors.full_messages
    end
    render 'admin/partials/_generic_js_response', layout: false
  end

  def remove_product
    UserProduct.where(user_id: @user.id, product_id: params[:product_id]).first.destroy
    @success = true
    flash[:notice] = 'Se eliminó el producto exitosamente'
    render 'admin/partials/_generic_js_response', layout: false
  end

  private
  def set_user
    if params[:user_id]
      @user = User.find params[:user_id]
    else
      @user = User.find params[:id]
    end
  end

  def user_params
    params.require(:user).permit!
  end
end
