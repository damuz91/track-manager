class Admin::TracksController < Admin::AdminController

  before_action :set_product
  before_action :set_track, except: [:index, :new, :create]

  def new
    @track = Track.new
    @track.product = @product
    render '_form', layout: false
  end

  def create
    @track = Track.new track_params
    @track.product_id = @product.id
    if @track.save
      @success = true
      flash[:notice] = 'Se creó el track'
    else
      @success = false
      @errors = @track.errors.full_messages
    end
    respond_to do |format|
			format.html do
				redirect_to admin_product_tracks_path(@product)
			end
			format.js  { render 'admin/partials/_generic_js_response', layout: false }
		end
  end

  def edit
    render '_form', layout: false
  end

  def update
    if @track.update_attributes track_params
      flash[:notice] = 'Se actualizó el track'
      @success = true
    else
      @success = false
      @errors = @track.errors.full_messages
    end
    respond_to do |format|
			format.html do
				redirect_to admin_product_tracks_path(@product)
			end
			format.js  { render 'admin/partials/_generic_js_response', layout: false }
		end
  end

  def destroy
    if @track.destroy
      @success = true
      flash[:notice] = 'Se eliminó el track'
    else
      @success = false
      @errors = @track.errors.full_messages
    end
    render 'admin/partials/_generic_js_response', layout: false
  end

  private
  def track_params
    params.require(:track).permit!
  end

  def set_product
    @product = Product.find params[:product_id]
  end

  def set_track
    @track = Track.find params[:id]
  end
end
