class Admin::ProductsController < Admin::AdminController

  before_action :set_product, except: [:index, :new, :create]

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    render '_form', layout: false
  end

  def create
    @product = Product.new product_params
    if @product.save
      flash[:notice] = 'Se creó el producto'
			@success = true
    else
      @success = false
      @errors = @product.errors.full_messages
      flash[:notice] = @errors
    end

    respond_to do |format|
			format.html do
				redirect_to admin_products_path
			end
			format.js  { render 'admin/partials/_generic_js_response', layout: false }
		end
  end

  def edit
    render '_form', layout: false
  end

  def update
    if @product.update_attributes product_params
      @success = true
      flash[:notice] = 'Se actualizó el producto'
    else
      @success = false
      @errors = @product.errors.full_messages
    end
    respond_to do |format|
			format.html do
				redirect_to admin_product_path(@product)
			end
			format.js  { render 'admin/partials/_generic_js_response', layout: false }
		end
  end

  def destroy
    @success = true
    render 'admin/partials/_generic_js_response', layout: false
  end

  private
  def set_product
    @product = Product.find params[:id]
  end

  def product_params
    params.require(:product).permit!
  end

end
