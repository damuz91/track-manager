class ApiController < ApplicationController

	skip_before_action :verify_authenticity_token
	

	def check_session
		token = request.headers[:token]

		if token.nil?
			render json: Utils::Response.get_error("Token expected"), status: :unauthorized and return
		end

		begin
			decoded_token = JWT.decode token, ENV['TOKEN_SECRET_SIGN'], true, { :algorithm => 'HS256' }
			success = true
			payload = decoded_token[0]
		rescue JWT::ImmatureSignature
			puts "-> Immature signature token detected"
			response = Utils::Response.get_error("Token with immature signature detected.")
			status = 401
		rescue JWT::ExpiredSignature
			puts "-> Expired token detected"
			response = Utils::Response.get_error("Expired Token")
			status = 401
		rescue Exception => e
			puts "-> Exception came up #{e} -> Token: #{token}"
			response = Utils::Response.get_error("An error has ocurred while decrypting the token.")
			status = 401
		end

		if !response.nil? and !status.nil?
			render json: response, status: status
		else
			puts "-> Looking for user with uid #{payload['uid']}"
			@current_user = User.find_by(id: payload['uid'])
			if @current_user.nil?
				response = Utils::Response.get_error("User not found with provided token")
				status = 404
				render json: response, status: status
			else
				# Should ask clients to include their device information like culture, timezone and screensize!!
				# @current_user.request = request.headers
			end
		end
	end

end