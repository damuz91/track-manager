class Product < ApplicationRecord

	has_many :tracks
	validates :name, :status, presence: true

	enum status: [:active, :archived, :unpublished]
	#enum product_type: [:free, :structured]

	has_many :user_products
	has_many :users, through: :user_products

  has_attached_file :guide
  validates_attachment_content_type :guide, content_type: ['application/pdf']

	# Renders a version of the cover_url according to the screen size
	def cover_url(screen_size=nil)
		# TODO: Implement
		"https://www.abc.es/media/play/2017/09/28/avatar-kVmB--620x349@abc.jpeg"
	end

end
