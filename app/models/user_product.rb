class UserProduct < ApplicationRecord
  	belongs_to :user
  	belongs_to :product

  	enum status: [:active, :archived]

  	def self.user_owns_product?(user_id,product_id)
  		UserProduct.where(user_id: user_id, product_id: product_id, status: UserProduct.statuses[:active]).any?
  	end
end
