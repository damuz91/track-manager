class User < ApplicationRecord
	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	validates :first_name, :last_name, :email, :role, :status, presence: true

	attr_accessor :request_info

	enum status: [:active, :archived]
	enum role: [:admin, :user]

	has_many :user_products
	has_many :products, through: :user_products

	def send_reset_password_instructions
		rescue "Implement me"
	end

  def full_name
    "#{first_name} #{last_name}"
  end

end
