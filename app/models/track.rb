class Track < ApplicationRecord
  	belongs_to :product
  	validates :status, :track_type, :product_id, presence: true
  	enum track_type: [:heatup, :box, :kick, :power, :combat, :superbox, :interactive, :relaxing, :misc, :sequence, :free]
  	enum status: [:active, :archived, :unpublished]

    has_attached_file :file
    validates_attachment_content_type :file, content_type: ['audio/mpeg', 'audio/x-mpeg', 'audio/mp3', 'audio/x-mp3', 'audio/mpeg3', 'audio/x-mpeg3', 'audio/mpg', 'audio/x-mpg', 'audio/x-mpegaudio']

end
