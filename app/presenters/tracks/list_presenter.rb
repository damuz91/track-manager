class Tracks::ListPresenter

	# Presents a track in a list
	# Params:
	# track*
	attr_accessor :id, :track_type, :duration

	def initialize(track)
		@id = track.id
		@duration = Utils::Tools.seconds_to_time(track.duration)
		@track_type = track.track_type
	end

end