class Products::ListPresenter

	# Presents a product in a list
	# Params:
	# product*
	# current_user*
	attr_accessor :id, :name, :owned, :cover_url, :product_type, :tracks_count

	def initialize(product,current_user)
		@id = product.id
		@name = product.name
		@owned = UserProduct.user_owns_product?(current_user.id,product.id) # TODO: Refactor to avoid n+1
		@cover_url = product.cover_url
		@product_type = product.product_type
		@tracks_count = product.tracks.where(status: Track.statuses[:active]).count
	end

end