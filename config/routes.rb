Rails.application.routes.draw do

  root to: 'home#index'
  namespace :admin do
    get '/' => 'home#index'
    resources :products do
      resources :tracks
    end
    resources :users do
      get 'products'
      get 'all_products'
      post 'assign_product'
      delete 'remove_product'
    end
  end

  apipie
  devise_for :users
	namespace :api do
		post 'login' => 'sessions#create'
    post 'register' => 'registrations#create'
    post 'forgot-password' => 'sessions#forgot_password'

    get 'products' => 'products#index'
    get 'tracks' => 'tracks#index'

    get 'track-detail' => 'tracks#show'
	end

end
